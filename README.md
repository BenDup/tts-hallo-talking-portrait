---
title: TTS x Hallo Talking Portrait
emoji: 👋
sdk: gradio
sdk_version: 4.37.1
app_file: app.py
pinned: false
suggested_hardware: a10g-large
short_description: 'Generate Talking avatars from Text-to-Speech '
colorFrom: indigo
colorTo: blue
---